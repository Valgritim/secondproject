package exoFunctions;

public class WriteFunctions {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	//Une fonction qui renvoie la plus grande de deux valeurs de type int.
	public static int plusGrande(int val1, int val2) {
		int result = Math.max(val1, val2);
		return result;
	}
	//Une fonction qui r�p�te un m�me mot un certain nombre de fois au choix.
	public static String repeat(int nbrFois, String mot) {
		String repeated = mot;
		for(int i = 0; i < nbrFois; i++) {			
			repeated += mot;			
		}		
		return repeated;
	}
//Une fonction, construite � partir de la fonction Math.random, qui tire au sort un nombre entier entre deux bornes donn�es en arguments.
	
	public static int tirageSort(int min, int max) {
		int range = max-min;
		int tirage = (int)(Math.random()*range) + min;
		return tirage;
	}
	
	//Une fonction qui teste si un tableau contient une valeur sp�cifique
	
	public static boolean testValeur(int[] tab, int cle) {
		boolean test = false;
		for(int valeur:tab) {
			if(valeur == cle) {
				test = true;
				break;
			}
		}
		return test;
	}
}
