package calculatrice;

import java.util.Scanner;

public class Calculatrice {

	public static void main(String[] args) {
		
		int choix;
		do {

			Scanner scan = new Scanner(System.in);
			System.out.println("\n"+"         MENU PRINCIPAL");
			System.out.println();
			System.out.println("   1. Additionner deux nombres");
			System.out.println();
			System.out.println("   2. Soustraire deux nombres");
			System.out.println();
			System.out.println("   3. Multiplier deux nombres");
			System.out.println();
			System.out.println("   4. Diviser deux nombres");
			System.out.println();
			System.out.println("   0. Quitter");
			System.out.println();
			System.out.println("   Faites votre choix : ");
			choix = scan.nextInt();
			
			if(choix == 0) break;
			
			if(choix < 4) {
				System.out.println("Entrez le nombre a : ");
				int a = scan.nextInt();
				System.out.println("Entrez le nombre b : ");
				int b = scan.nextInt();		
				if(choix == 4 && b ==0) {
					System.out.println("\n" + "le nombre b est nul: la division est impossible");
					continue;
				} 
				System.out.println("le résultat de l'opération est:" + " " + operation(choix,a,b));
				
			} else System.out.println("\n"+"Cette opération n'existe pas! Recommencez!");
			
		} while (choix != 0);
			System.out.println();
			System.out.println("Au revoir!");

	}


	public static int operation(int choix, int a, int b) {
		int result=0;
		
		switch(choix) {
		
		case 1: 
			result = a + b;
			System.out.println("votre addition");
			break;
			
		case 2:
			 result = a - b;
			 System.out.println("votre soustraction");
			break;
				
		case 3:
			result = a * b;
			System.out.println("votre multiplication");
			break;
		case 4:
				result = a / b;
				System.out.println("votre division");
			break;
				
			
		}
		
		return result;
	}		

}
