package Exercices;

public class Age {

	public static void main(String[] args) {
		
		String bob = "Bob";
		String daniel = "Daniel";
		String eric = "Eric";
		
		int ageBob = 18;
		int ageDaniel = 8;
		int ageEric = 12;
		
//		if(ageBob >= 18) {
//			System.out.println(bob + " est un adulte");			
//		} else if(ageBob < 18 && ageBob > 10) {
//			System.out.println(bob + " est un ado");
//		} else {
//			System.out.println(bob + " est un enfant");
//		}
//		
//		if(ageDaniel >= 18) {
//			System.out.println(daniel + " est un adulte");			
//		} else if(ageDaniel < 18 && ageDaniel > 10) {
//			System.out.println(daniel + " est un ado");
//		} else {
//			System.out.println(daniel + " est un enfant");
//		}
//		
//		if(ageEric >= 18) {
//			System.out.println(eric + " est un adulte");			
//		} else if(ageEric < 18 && ageEric > 10) {
//			System.out.println(eric + " est un ado");
//		} else {
//			System.out.println(eric + " est un enfant");
//		}
		result(daniel,ageDaniel);
		result(bob,ageBob);
		result(eric,ageEric);
		
	}
	
	public static void result(String name, int age) {

		if (age >= 18) {
			System.out.println(name + " est un adulte");
		} else if (age < 18 && age > 10) {
			System.out.println(name + " est un ado");
		} else {
			System.out.println(name + " est un enfant");
		}
		
	}
	

}
