package fibonacci;

import java.util.Scanner;

public class Fibonacci {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scan = new Scanner(System.in);
		System.out.println("Quel �l�ment de la suite de Fibonacci voulez-vous connaitre?:");
		int n = scan.nextInt();
		
		System.out.print("Le " + n + "�me �l�ment de la suite est: " + fibo(n));
		
	}
	
	public static int fibo(int n) {
		int result;

		if(n == 1 || n == 2) {
			result = 1;
		} else {
			result = fibo(n-1)+fibo(n-2);
		}
		return result;
	}
}
