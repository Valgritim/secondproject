package tp2;


import java.util.Scanner;

public class PartiesGagnees {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);		
		System.out.println("Combien de parties avez-vous jou�?:");
		int nbParties = scan.nextInt();
		
//		System.out.println("Score joueur 1:" + Arrays.toString(firstPlayer(nbParties)));
//		System.out.println("Score joueur 2:" + Arrays.toString(secondPlayer(nbParties)));
		
		int[] scorePlayer1 = firstPlayer(nbParties);
		int [] scorePlayer2 = secondPlayer(nbParties);
		boolean result = gamesWon(scorePlayer1,scorePlayer2,nbParties);
		if(result) {
			System.out.print("Le joueur 1 est gagnant");
		} else {
			System.out.print("Le joueur 1 est perdant");
		}
		
		
		
		scan.close();
	}
	public static int[] firstPlayer(int partiesNr) {
		Scanner scan = new Scanner(System.in);
		int[]score1 = new int[partiesNr];
		
		for(int i =0; i < partiesNr; i++) {
			System.out.println("Joueur n�1,entrez le score N� " + (i+1));
			int enter = scan.nextInt();
			score1[i] = enter;
		}
		
		return score1;		
	}
	public static int[] secondPlayer(int partiesNr) {
		Scanner scan = new Scanner(System.in);
		int[]score2 = new int[partiesNr];
		
		for(int j =0; j < partiesNr; j++) {
			System.out.println("Joueur n�2, entrez le score N� " + (j+1));
			int enter2 = scan.nextInt();
			score2[j] = enter2;
		}
		
		return score2;		
	}
	
	public static boolean gamesWon(int[] scorePlayer1, int[] scorePlayer2, int nbParties) {
		boolean gagnant = false;
		int totalWon = 0;
		
		for(int i = 0; i < nbParties; i++) {			
			if(scorePlayer1[i] > scorePlayer2[i]) {
				totalWon++;				
			} else {
				totalWon = 0;					
			}
		}	
		if(totalWon > (nbParties/2)){
			 gagnant = true;			
		} else {
			gagnant = false;
		}		
		return gagnant;	
	}
}
