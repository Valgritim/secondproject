package exos;

import java.util.Scanner;

public class Saison {

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);
		System.out.println("Choisissez un entier de 1 � 12: ");
		int mois = scan.nextInt();
		
		calcul(mois);
				
		scan.close();
	}

	public static void calcul(int mois) {
		if (mois < 1 || mois > 12) {
			System.out.println("Entr�e erronn�e!!");
		} else if (mois >= 1 && mois <= 3) {
			System.out.println("On est en Hiver");
		} else if (mois >= 4 && mois <= 6) {
			System.out.println("On est au Printemps");
		} else if (mois >= 7 && mois <= 9) {
			System.out.println("On est en Et�");
		} else {
			System.out.println("On est en Automne");
		}
	}
}
